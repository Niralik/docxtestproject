﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
  
    public class DocxDbContext:DbContext
    {
        public DbSet<DocxFile> DocxFiles { get; set; }

        public DocxDbContext()
            : base("DocxDbContext")
        {
            
        }

        static DocxDbContext()
        {
            Database.SetInitializer(new DocxDbInitializer());
        }
    }

    internal class DocxDbInitializer : DropCreateDatabaseIfModelChanges<DocxDbContext>
    {
        
    }

}
