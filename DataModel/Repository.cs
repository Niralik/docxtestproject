﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
   public class DocxFileRepository :  IDocxFileRepository
    {
         public DocxDbContext Context { get; private set; }

       public DocxFileRepository()
       {
           Context = new DocxDbContext();
       }
        public IQueryable<DocxFile> All
        {
            get { return Context.DocxFiles; }
        }

        public IQueryable<DocxFile> AllIncluding(params Expression<Func<DocxFile, object>>[] includeProperties)
        {
            IQueryable<DocxFile> query = Context.DocxFiles;
            foreach (var includeProperty in includeProperties) {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public DocxFile Find(int id)
        {
            return Context.DocxFiles.Find(id);
        }

        public void InsertOrUpdate(DocxFile docxFile)
        {
            if (docxFile.Id == default(int)) {
                // New entity
                Context.DocxFiles.Add(docxFile);
            } else {
                // Existing entity
                Context.Entry(docxFile).State = EntityState.Modified;
            }
        }

        public void Delete(int id)
        {
            var blog = Context.DocxFiles.Find(id);
            Context.DocxFiles.Remove(blog);
        }

        public void Save()
        {
            Context.SaveChanges();
        }

        public void Dispose() 
        {
            Context.Dispose();
        }
    }

    public interface IDocxFileRepository : IDisposable
    {
        IQueryable<DocxFile> All { get; }
        IQueryable<DocxFile> AllIncluding(params Expression<Func<DocxFile, object>>[] includeProperties);
        DocxFile Find(int id);
        void InsertOrUpdate(DocxFile blog);
        void Delete(int id);
        void Save();
    }
}
