﻿using System.Web.Optimization;

namespace DocxTestProject
{
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js",
                "~" + Links.Scripts.jquery_bootstrap_wizard_js,
                       "~" + Links.Scripts.jquery_validate_js,
                 "~" + Links.Scripts.jquery_validate_unobtrusive_js,

                "~" + Links.Scripts.jquery_unobtrusive_ajax_js,
                "~" + Links.Scripts.jquery_ui_1_9_0_js,
                "~" + Links.Scripts.jQuery_FileUpload.jquery_iframe_transport_js,
                "~" + Links.Scripts.jQuery_FileUpload.jquery_fileupload_js,
                "~" + Links.Scripts.jQuery_FileUpload.jquery_fileupload_process_js,
                "~" + Links.Scripts.jQuery_FileUpload.jquery_fileupload_validate_js,
                "~" + Links.Scripts.toastr_js,
                     "~" + Links.Scripts.idealforms.localization.idealforms_l8on_ru_js,
                   "~" + Links.Scripts.idealforms.idealforms_ts,
                "~" + Links.Scripts.Site.CustomScripts.jquery_extensions_ts,
                 "~" + Links.Scripts.Site.edit_document_ts,
                "~" + Links.Scripts.Site.wizard_ts,
                "~" + Links.Scripts.Site.uploader_ts,
                "~" + Links.Scripts.Site.site_js));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/site.css",
                "~" + Links.Content.toastr_css,
                "~" + Links.Content.jQuery_FileUpload.css.jquery_fileupload_css));
        }
    }
}