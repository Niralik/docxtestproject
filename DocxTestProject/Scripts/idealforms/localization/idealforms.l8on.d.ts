interface IErrors {
    required: string;
    digits: string;
    name: string;
    email: string;
    username: string;
    weakpass: string;
    pass: string;
    strongpass: string;
    phone: string;
    zip: string;
    url: string;
    number: string;
    range: string;
    min: string;
    max: string;
    minoption: string;
    maxoption: string;
    minmax: string;
    select: string;
    extension: string;
    equalto: string;
    date: string;
    birthdate: string;
    snils: string;
    numorder: string;
    checkLocation: string;
    checkInstitute: string;
    checkReferer: string;
    ajax: string;
    internalServerError: string;
}
interface ICustomInputErrors {
    Email: {
        ajax: string;
        ajaxError: string;
    };
    PhoneNumber: {
        min: string;
        max: string;
    };
}
interface IIdealformI18N {
    customInputErrors: ICustomInputErrors;
    errors: IErrors;
}

declare var IdealformI18N;