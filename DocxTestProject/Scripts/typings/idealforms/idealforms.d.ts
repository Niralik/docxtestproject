﻿interface IIdealformOptions {
    /**CSS класс полей к которым будет применена валидация*/
    fieldClass?: string;
    /**СSS класс див контейнера в котором будет хранится ошибка валидации */
    errorClass?: string;
    /** Флаг отмечающий будут ли показываться иконки валидации - по умолчанию true*/
    hideValidateIcons?: boolean;
    /** HTML тег иконки валидации*/
    iconHtml?: string;
    iconBaseClass?: string;
    iconExclamationClass?: string;
    iconSuccessClass?: string;
    iconValidatingClass?: string;
    invalidClass?: string;
    validClass?: string;
    /**Флаг обозначающий что форма загрузится без валидирования существующих полей*/
    silentLoad?: boolean;
    adaptiveWidth?: number;
    /**Событие возникающие после валидации текущего поля */
    onValidate($currentInput: JQuery, ruleName: string, isValid: boolean): any;
    /**Событие возникающие при сабмите формы */
    onSubmit(numOfInvalid: number, event: Event): boolean;
    /**Список полей с именами правил валидации*/
    rules: IUserRules;
    errors?: Object;
}
interface IUserRules {
    [inputName: string]: string;
}
interface IIdealforms {
    Initialization(): void;
    AddValidationRules(rules: any): void;
    GetInvalidFields(): JQuery;
    FocusToFirstInvalidInput(): void;
    IsValid($input?: JQuery): boolean;
    Reset($input?: JQuery): void;
    Validate($input: JQuery): boolean;
    GetValidateField($input: JQuery): JQuery;
}
interface IRequests {
    [inputName: string]: JQueryXHR;
}

interface JQuery {
    idealforms(options: IIdealformOptions): IIdealforms;
}

interface JQueryStatic {
    idealforms: IIdealforms;
}