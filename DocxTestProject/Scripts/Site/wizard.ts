﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/jquery.bootstrap.wizard/jquery.bootstrap.wizard.d.ts" />
class DocxWizard {
    private static $wizard = $("#wizard");
    private $editDocumentForm = $('.form-wizard .edit-step .edit-document-form');
    constructor() {
        DocxWizard.$wizard.bootstrapWizard({
            'tabClass': 'nav nav-pills',
            onNext: (activeTab: any, navigation: any, currentIndex: number): boolean => {
                if (this.checkIsCompleteStep()) {
                    this.setActivityForStepsTabs($(navigation), currentIndex);
                    return true;
                } else {
                    if (this.checkIsUploadStep()) {
                        toastr.warning("Чтобы перейти на следующий этап необоходимо загрузить какой либо документ в docx формате");
                    }
                if (this.checkIsEditStep() && this.checkIsValidEditForm()) {
                    this.$editDocumentForm.submit();
                }
                    return false;
                }
            },
            onTabClick: (activeTab: any, navigation: any, currentIndex: number): boolean => {
                return false;
            },
            onTabChange: (activeTab: any, navigation: any, currentIndex: number) => {
                this.setHeightOfFormWizardByContent();
            },
        });
        this.$editDocumentForm.change(() => {
            DocxWizard.FailedStep();
        });
    }
    private $formWizard = $('.form-wizard');
    private checkIsUploadStep() {
        var $activeStep = this.$formWizard.find('.step.active');
        return $activeStep.is('.upload-step');
    }
    private checkIsEditStep() {
        var $activeStep = this.$formWizard.find('.step.active');
        return $activeStep.is('.edit-step');
    }
    private checkIsValidEditForm() {
        var $activeStep = this.$formWizard.find('.step.active');
        var editForm = $activeStep.find('.idealforms');
        if (editForm) {
            var idealform = <IIdealforms>editForm.data('idealforms');
            idealform.FocusToFirstInvalidInput();
            return idealform.GetInvalidFields().length == 0;
        }
        return false;
    }

    private setHeightOfFormWizardByContent() {
        var $activeStep = this.$formWizard.find('.step.active');
        var heightActiveStep = $activeStep.height();
        this.$formWizard.height(heightActiveStep);
    }
    private setActivityForStepsTabs(navigation: JQuery, currentIndex: number) {
        var stepsTabs = navigation.find('li');
        stepsTabs.filter((index: number, stepTab) => { return index < currentIndex; }).addClass('active-past');
    }

    private checkIsCompleteStep(): boolean {
        var activeStep = $('.form-wizard .step.active');
        var isCompleteStep = parseBool(activeStep.find('#isCompleteStep').val());
        return isCompleteStep;
    }

    static NextStep(): void {
        $("#wizard").bootstrapWizard('next');
    }

    static CompleteStep(): void {
        var activeStep = $('.form-wizard .step.active');
        activeStep.find('#isCompleteStep').val('true');
    }

    static FailedStep(): void {
        var activeStep = $('.form-wizard .step.active');
        activeStep.find('#isCompleteStep').val('false');
    }
}