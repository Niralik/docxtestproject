﻿/**
* Интерфейс описывающий ответ с сервера аналогичен классу AlertWrapper на сервере
*/
interface IServerResponse {
    Successed: boolean;
    Code?: number;
    RelatedData?: any;
    AlertSections: IAlertSection[];
}
/**
* Интерфейс описывает одну секцию представляющию опвещение определенного типа
*/
interface IAlertSection {
    TypeAlert: TypeOfAlert;
    Messages?: string[];
    Message: string;
}
///<var>Тип оповещения</var>
///Порядок должен совпадать с перечеслением на сервере
enum TypeOfAlert {
    Info,
    Warning,
    Success,
    Error
};