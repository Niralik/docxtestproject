﻿

///<var>Тип оповещения</var>
///Порядок должен совпадать с перечеслением на сервере
var TypeOfAlert;
(function (TypeOfAlert) {
    TypeOfAlert[TypeOfAlert["Info"] = 0] = "Info";
    TypeOfAlert[TypeOfAlert["Warning"] = 1] = "Warning";
    TypeOfAlert[TypeOfAlert["Success"] = 2] = "Success";
    TypeOfAlert[TypeOfAlert["Error"] = 3] = "Error";
})(TypeOfAlert || (TypeOfAlert = {}));
;
//# sourceMappingURL=IServerResponse.js.map
