﻿class DocxUploader {
    private $fileuploadInput = $('#fileupload');
    private $progress = $('.progress');
    constructor() {
        this.intitalizeFileUploader();
    }

    private intitalizeFileUploader() {
        this.$fileuploadInput.fileupload({
            dataType: 'json',
            acceptFileTypes: /(\.|\/)(doc?x)$/i,
            done: (e, data) => {
                this.$progress.fadeOut(300);
                $('.fileinput-button').removeClass('disabled');
                var serverResponse = <IServerResponse>data.result;
                if (serverResponse.Successed) {
                    toastr.success('Файл успешно загружен');
                    DocxWizard.CompleteStep();
                    var editDocument = new EditDocument(serverResponse.RelatedData);
                    editDocument.initialize();
                    DocxWizard.NextStep();
                } else {
                    DocxWizard.FailedStep();
                    toastr.error('При разборе файла произошла ошибка, проверьте формат загружаемого документа');
                }
            },
            progressall: (e, data) => {
                var progress = parseInt((data.loaded / data.total * 100).toString(10));
                if (this.$progress.is(":hidden")) {
                    this.$progress.fadeIn(300);
                }
                $('.progress-bar').css(
                    'width',
                    progress + '%'
                );
            },
        }).on('fileuploadadd', (e, data) => {
                $('.fileinput-button').addClass('disabled');
        }).on('fileuploadfail', (e, data) => {
            $('.fileinput-button').removeClass('disabled');
            toastr.error('Внутренняя ошибка сервера');
            this.$progress.fadeOut(300);
            }).on('fileuploadprocessalways', (e, data) => {
                var index = data.index,
                    file = data.files[index];
                if (file.error) {
                    $('.fileinput-button').removeClass('disabled');
                    toastr.error(file.name + ' - имеет неверный формат.', 'Ошибка!');
                }
            }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');;
    }
}