﻿/// <reference path="../idealforms/idealforms.ts" />

var EditDocument = (function () {
    function EditDocument(docxModel) {
        this.docxModel = docxModel;
        this.$editDocumentForm = $('.form-wizard .edit-step .edit-document-form');
        this.urlToAction = this.$editDocumentForm.attr('action');
        this.rules = {};
        this.constructFormGroups(docxModel);
    }
    EditDocument.prototype.initialize = function () {
        this.$editDocumentForm.empty().append(this.formGroups);
        this.initializeIdealformValidation();
    };
    EditDocument.prototype.constructFormGroups = function (docxModel) {
        var _this = this;
        this.formGroups = $('<div></div>');
        var nameInput = $('<input/>', {
            "class": "form-control",
            "id": "Name",
            "name": "Name",
            "value": docxModel.Name,
            "type": "hidden"
        });
        this.formGroups.append(nameInput);
        $.each(docxModel.Fields, function (fieldKey, fieldValue) {
            var labelOfField = $("<label></label>").text(fieldKey);
            var inputOfField = $('<input/>', {
                "class": "form-control",
                "id": fieldKey,
                "name": fieldKey
            });
            inputOfField.attr('required', 'required');
            var formGroup = $('<div></div>', {
                "class": "form-group"
            });
            var fieldIdel = $('<div></div>', {
                "class": "field-ideal"
            });
            var spanError = $('<span></span>', {
                "class": "error"
            });
            formGroup.append(labelOfField);
            fieldIdel.append(inputOfField);
            fieldIdel.append(spanError);
            formGroup.append(fieldIdel);
            _this.rules[fieldKey] = 'required';
            _this.formGroups.append(formGroup);
        });
    };
    EditDocument.prototype.initializeIdealformValidation = function () {
        var _this = this;
        this.$editDocumentForm.idealforms({
            rules: this.rules,
            adaptiveWidth: 545,
            onValidate: function (item, e, valid) {
                if (e === "ajax" && valid) {
                }
            },
            onSubmit: function (invalid, e) {
                e.preventDefault();
                if (invalid == 0) {
                    _this.sendDataOfFormByAjaxToServer();
                }
                return false;
            }
        });
    };
    EditDocument.prototype.sendDataOfFormByAjaxToServer = function () {
        $.ajax({
            type: 'POST',
            url: this.urlToAction,
            data: this.generateEditDocumentData(),
            beforeSend: function () {
                //toastr.info("Сохраняем изменения");
            },
            success: function (response) {
                if (response.Successed) {
                    toastr.success("Поздравляем, изменения сохранены, можете скачать файл по ссылке");
                    DocxWizard.CompleteStep();
                    DocxWizard.NextStep();
                    $('#docxDownload').attr('href', response.RelatedData);
                } else {
                    DocxWizard.FailedStep();
                    toastr.error("Внутренняя ошибка сервера");
                }
            },
            error: function (e) {
                if (e.statusText != "abort") {
                    toastr.error("Внутренняя ошибка сервера");
                }
            }
        });
    };
    EditDocument.prototype.generateEditDocumentData = function () {
        var dataToServer = {};
        var dataArray = this.$editDocumentForm.serializeArray();
        var name = dataArray[0].value;
        $.each(dataArray, function (key, object) {
            if (key == 0)
                return;
            dataToServer[object.name] = object.value;
        });
        return {
            Name: name,
            Fields: dataToServer
        };
    };
    return EditDocument;
})();
//# sourceMappingURL=edit-document.js.map
