﻿var DocxUploader = (function () {
    function DocxUploader() {
        this.$fileuploadInput = $('#fileupload');
        this.$progress = $('.progress');
        this.intitalizeFileUploader();
    }
    DocxUploader.prototype.intitalizeFileUploader = function () {
        var _this = this;
        this.$fileuploadInput.fileupload({
            dataType: 'json',
            acceptFileTypes: /(\.|\/)(doc?x)$/i,
            done: function (e, data) {
                _this.$progress.fadeOut(300);
                $('.fileinput-button').removeClass('disabled');
                var serverResponse = data.result;
                if (serverResponse.Successed) {
                    toastr.success('Файл успешно загружен');
                    DocxWizard.CompleteStep();
                    var editDocument = new EditDocument(serverResponse.RelatedData);
                    editDocument.initialize();
                    DocxWizard.NextStep();
                } else {
                    DocxWizard.FailedStep();
                    toastr.error('При разборе файла произошла ошибка, проверьте формат загружаемого документа');
                }
            },
            progressall: function (e, data) {
                var progress = parseInt((data.loaded / data.total * 100).toString(10));
                if (_this.$progress.is(":hidden")) {
                    _this.$progress.fadeIn(300);
                }
                $('.progress-bar').css('width', progress + '%');
            }
        }).on('fileuploadadd', function (e, data) {
            $('.fileinput-button').addClass('disabled');
        }).on('fileuploadfail', function (e, data) {
            $('.fileinput-button').removeClass('disabled');
            toastr.error('Внутренняя ошибка сервера');
            _this.$progress.fadeOut(300);
        }).on('fileuploadprocessalways', function (e, data) {
            var index = data.index, file = data.files[index];
            if (file.error) {
                $('.fileinput-button').removeClass('disabled');
                toastr.error(file.name + ' - имеет неверный формат.', 'Ошибка!');
            }
        }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
        ;
    };
    return DocxUploader;
})();
//# sourceMappingURL=uploader.js.map
