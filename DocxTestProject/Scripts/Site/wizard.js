﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/jquery.bootstrap.wizard/jquery.bootstrap.wizard.d.ts" />
var DocxWizard = (function () {
    function DocxWizard() {
        var _this = this;
        this.$editDocumentForm = $('.form-wizard .edit-step .edit-document-form');
        this.$formWizard = $('.form-wizard');
        DocxWizard.$wizard.bootstrapWizard({
            'tabClass': 'nav nav-pills',
            onNext: function (activeTab, navigation, currentIndex) {
                if (_this.checkIsCompleteStep()) {
                    _this.setActivityForStepsTabs($(navigation), currentIndex);
                    return true;
                } else {
                    if (_this.checkIsUploadStep()) {
                        toastr.warning("Чтобы перейти на следующий этап необоходимо загрузить какой либо документ в docx формате");
                    }
                    if (_this.checkIsEditStep() && _this.checkIsValidEditForm()) {
                        _this.$editDocumentForm.submit();
                    }
                    return false;
                }
            },
            onTabClick: function (activeTab, navigation, currentIndex) {
                return false;
            },
            onTabChange: function (activeTab, navigation, currentIndex) {
                _this.setHeightOfFormWizardByContent();
            }
        });
        this.$editDocumentForm.change(function () {
            DocxWizard.FailedStep();
        });
    }
    DocxWizard.prototype.checkIsUploadStep = function () {
        var $activeStep = this.$formWizard.find('.step.active');
        return $activeStep.is('.upload-step');
    };
    DocxWizard.prototype.checkIsEditStep = function () {
        var $activeStep = this.$formWizard.find('.step.active');
        return $activeStep.is('.edit-step');
    };
    DocxWizard.prototype.checkIsValidEditForm = function () {
        var $activeStep = this.$formWizard.find('.step.active');
        var editForm = $activeStep.find('.idealforms');
        if (editForm) {
            var idealform = editForm.data('idealforms');
            idealform.FocusToFirstInvalidInput();
            return idealform.GetInvalidFields().length == 0;
        }
        return false;
    };

    DocxWizard.prototype.setHeightOfFormWizardByContent = function () {
        var $activeStep = this.$formWizard.find('.step.active');
        var heightActiveStep = $activeStep.height();
        this.$formWizard.height(heightActiveStep);
    };
    DocxWizard.prototype.setActivityForStepsTabs = function (navigation, currentIndex) {
        var stepsTabs = navigation.find('li');
        stepsTabs.filter(function (index, stepTab) {
            return index < currentIndex;
        }).addClass('active-past');
    };

    DocxWizard.prototype.checkIsCompleteStep = function () {
        var activeStep = $('.form-wizard .step.active');
        var isCompleteStep = parseBool(activeStep.find('#isCompleteStep').val());
        return isCompleteStep;
    };

    DocxWizard.NextStep = function () {
        $("#wizard").bootstrapWizard('next');
    };

    DocxWizard.CompleteStep = function () {
        var activeStep = $('.form-wizard .step.active');
        activeStep.find('#isCompleteStep').val('true');
    };

    DocxWizard.FailedStep = function () {
        var activeStep = $('.form-wizard .step.active');
        activeStep.find('#isCompleteStep').val('false');
    };
    DocxWizard.$wizard = $("#wizard");
    return DocxWizard;
})();
//# sourceMappingURL=wizard.js.map
