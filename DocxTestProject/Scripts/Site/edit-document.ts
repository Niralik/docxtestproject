﻿/// <reference path="../idealforms/idealforms.ts" />
interface IDocxModel {
    Name: string;
    Fields: Object[];
}

class EditDocument{
    private formGroups: JQuery;
    private $editDocumentForm = $('.form-wizard .edit-step .edit-document-form');
    private urlToAction = this.$editDocumentForm.attr('action');
    private rules: IUserRules = {};
    constructor(private docxModel: IDocxModel) {
        this.constructFormGroups(docxModel);
        }
    public initialize() {
        this.$editDocumentForm.empty().append(this.formGroups);
        this.initializeIdealformValidation();
    }
    private constructFormGroups(docxModel: IDocxModel) {
        this.formGroups = $('<div></div>');
        var nameInput = $('<input/>', {
            "class": "form-control",
            "id": "Name",
            "name": "Name",
            "value": docxModel.Name,
            "type":"hidden"
        });
        this.formGroups.append(nameInput);
        $.each(docxModel.Fields, (fieldKey, fieldValue) => {
            var labelOfField = $("<label></label>").text(fieldKey);
            var inputOfField = $('<input/>', {
                "class": "form-control",
                "id": fieldKey,
                "name": fieldKey
            });
            inputOfField.attr('required', 'required');
            var formGroup = $('<div></div>', {
                "class": "form-group"
            });
            var fieldIdel = $('<div></div>', {
                "class": "field-ideal"
            });
            var spanError = $('<span></span>', {
                "class": "error"
            });
            formGroup.append(labelOfField);
            fieldIdel.append(inputOfField);
            fieldIdel.append(spanError);
            formGroup.append(fieldIdel);
            this.rules[fieldKey] = 'required';
            this.formGroups.append(formGroup);
        });
    }
    private initializeIdealformValidation() {
        this.$editDocumentForm.idealforms({
            rules: this.rules,
            adaptiveWidth: 545,
            onValidate: function(item, e, valid) {
                if (e === "ajax" && valid) {
                }
            },
            onSubmit: (invalid, e) => {
                e.preventDefault();
                if (invalid == 0) {
                    this.sendDataOfFormByAjaxToServer();
                }
                return false;
            }
        });
    }
    private sendDataOfFormByAjaxToServer() {
        $.ajax({
            type: 'POST',
            url: this.urlToAction,
            data: this.generateEditDocumentData(),
            beforeSend: () => {
                //toastr.info("Сохраняем изменения");
            },
            success: (response: IServerResponse) => {
                if (response.Successed) {
                    toastr.success("Поздравляем, изменения сохранены, можете скачать файл по ссылке");
                    DocxWizard.CompleteStep();
                    DocxWizard.NextStep();
                    $('#docxDownload').attr('href', response.RelatedData);
                } else {
                    DocxWizard.FailedStep();
                    toastr.error("Внутренняя ошибка сервера");
                }
            },
            error: (e: any) => {
                if (e.statusText != "abort") {
                    toastr.error("Внутренняя ошибка сервера");
                }
            }
        });
    }
    private generateEditDocumentData() {
        var dataToServer = {};
        var dataArray = this.$editDocumentForm.serializeArray();
        var name = dataArray[0].value;
        $.each(dataArray, (key, object) => {
            if (key == 0) return;
            dataToServer[object.name] = object.value;
        });
        return {
            Name: name,
            Fields: dataToServer
    }
    }
} 