﻿using DocxTestProject.Core.Alert;
using DocxTestProject.Models;
using Novacode;
using System;
using System.Web.Mvc;

namespace DocxTestProject.Controllers
{
    public partial class HomeController : BaseController
    {
        public virtual ActionResult Index()
        {
            var listUplodedDocxDocuments = StorageManager.GetUploadedDocxFiles();
            return View(listUplodedDocxDocuments);
        }

        public virtual ActionResult NewDocxDocument()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult EditDocxDocument(DocxModel docxModel)
        {
            try
            {
                var updatingFileStream = StorageManager.GetSourceFile(docxModel.Name);
                var docxModelOfStorageFile = new DocxModel(DocX.Load(updatingFileStream), docxModel.Name);
                updatingFileStream.Dispose();
                docxModelOfStorageFile.Update(docxModel);
                docxModelOfStorageFile.GetSource().SaveAs(StorageManager.GetPhisicalPathToFile(docxModel.Name));
                docxModelOfStorageFile.GetSource().Dispose();
                return AlertJson.Success(StorageManager.GetUrlToFile(docxModel.Name), 1);
            }
            catch (Exception)
            {
                return AlertJson.Error();
            }
        }
    }
}