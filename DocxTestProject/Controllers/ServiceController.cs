﻿using DocxTestProject.Core.Alert;
using DocxTestProject.Models;
using Novacode;
using System;
using System.Web.Mvc;

namespace DocxTestProject.Controllers
{
    public partial class ServiceController : BaseController
    {
        [HttpPost]
        public virtual ActionResult FileUpload()
        {
            try
            {
                var recievedFileFromBrowser = Request.Files[0];
                if (recievedFileFromBrowser == null) return AlertJson.Error();

                StorageManager.SaveSourceFile(recievedFileFromBrowser.InputStream, recievedFileFromBrowser.FileName);

                var docxViewModel = new DocxModel(DocX.Load(recievedFileFromBrowser.InputStream), recievedFileFromBrowser.FileName);

                return AlertJson.Success(docxViewModel, 1);
            }
            catch (Exception)
            {
                return AlertJson.Error();
            }
        }
    }
}