﻿using DocxTestProject.Core;
using System.Web.Mvc;

namespace DocxTestProject.Controllers
{
    public partial class BaseController : Controller
    {
        public LocalStorageManager StorageManager = new LocalStorageManager();
    }
}