﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DocxTestProject.Core.Alert
{
    public class AlertJson
    {
        public AlertJson()
        {
        }

        /// <summary>
        /// Метод формирует Json объект представляющий оповещение символизирующие Успех
        /// </summary>
        /// <param name="alertMessages">Список сообщений сопуствующий данному оповещению</param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        public static JsonResult Success(params string[] alertMessages)
        {
            return Compile(TypeOfAlert.Success, alertMessages);
        }

        public static JsonResult Success(object relatedData, int code)
        {
            return Compile(TypeOfAlert.Success, relatedData, code);
        }

        /// <summary>
        /// Метод формирует Json объект представляющий оповещение символизирующие Ошибку - без параметров по умолчанию вернет ошибку InternalServerError
        /// </summary>
        /// <param name="alertMessages">Список сообщений сопуствующий данному оповещению</param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        public static JsonResult Error(params string[] alertMessages)
        {
            return Compile(TypeOfAlert.Error, alertMessages);
        }

        /// <summary>
        /// Метод формирует Json объект представляющий Ошибки операций валидации модели
        /// </summary>
        /// <param name="modelStateCollection">Значения состояния модели полученные из этого свойства: <see cref="ModelState.Values"/></param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        public static JsonResult Error(ICollection<ModelState> modelStateCollection)
        {
            var errors = new List<string>();
            foreach (var modelState in modelStateCollection.Where(ms => ms.Errors != null))
            {
                errors.AddRange(modelState.Errors.Select(modelError => modelError.ErrorMessage).ToList());
            }

            var typeOfAlertMessage = TypeOfAlert.Success;
            if (errors.Any()) typeOfAlertMessage = TypeOfAlert.Error;

            return Compile(new AlertSection(typeOfAlertMessage, errors.ToArray()));
        }

        /// <summary>
        /// Метод формирует Json объект представляющий оповещение со списком ошибок сформированными системой Identity -
        /// если результат операции будет успешным - будет сформированное пустое сообщение с флагом <see cref="Successed = true"/>
        /// </summary>
        /// <param name="identityResult">Результат Identity операции</param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        //public static JsonResult Error(IdentityResult identityResult)
        //{
        //    var typeOfAlertMessage = TypeOfAlert.Error;
        //    if (identityResult.Succeeded) typeOfAlertMessage = TypeOfAlert.Success;
        //    return Compile(new AlertSection(typeOfAlertMessage, identityResult.Errors.ToArray()));
        //}
        public static JsonResult Error(object relatedData, int code)
        {
            return Compile(TypeOfAlert.Error, relatedData, code);
        }

        /// <summary>
        /// Метод формирует Json объект представляющий Предупреждающие оповещение
        /// </summary>
        /// <param name="alertMessages">Список сообщений сопуствующий данному оповещению</param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        public static JsonResult Warning(params string[] alertMessages)
        {
            return Compile(TypeOfAlert.Warning, alertMessages);
        }

        /// <summary>
        /// Метод формирует Json объект представляющий Информационное оповещение
        /// </summary>
        /// <param name="alertMessages">Список сообщений сопуствующий данному оповещению</param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        public static JsonResult Attention(params string[] alertMessages)
        {
            return Compile(TypeOfAlert.Info, alertMessages);
        }

        /// <summary>
        /// Метод упаковывает контейнеры оповещений в один json объект - притом общий флаг Successed установится в False - если среди контейнеров сообщений
        /// будет хоть один с типом <see cref="TypeOfAlert.Error"/>
        /// </summary>
        /// <param name="alertSections">Контейнеры оповещений</param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        public static JsonResult Compile(params AlertSection[] alertSections)
        {
            return Compile(null, null, alertSections);
        }

        public static JsonResult Compile(object relatedData, params AlertSection[] alertSections)
        {
            return Compile(relatedData, null, alertSections);
        }

        public static JsonResult Compile(int code, params AlertSection[] alertSections)
        {
            return Compile(null, code, alertSections);
        }

        public static JsonResult Compile(object relatedData, int? code, params AlertSection[] alertSections)
        {
            var alert = new AlertsWrapper(new List<AlertSection>(alertSections));
            alert.RelatedData = relatedData;
            alert.Code = code.HasValue ? code.Value : 0;
            return new JsonResult { Data = alert };
        }

        public static JsonResult Compile(TypeOfAlert typeAlert, object relatedData, int code)
        {
            return Compile(typeAlert, relatedData, code, null);
        }

        /// <summary>
        /// Метод формирует Json объект представляющий оповещение заданного типа
        /// </summary>
        /// <param name="typeAlert">Тип оповещения</param>
        /// <param name="alertMessages">Список сообщений сопуствующий данному оповещению</param>
        /// <returns>Json объект вида: {Successed: bool, AlertSections:[{ TypeAlert: TypeOfAlert,
        /// Messages: string[]}...etc]}</returns>
        public static JsonResult Compile(TypeOfAlert typeAlert, params string[] alertMessages)
        {
            return Compile(null, 0, new AlertSection(typeAlert, alertMessages));
        }

        public static JsonResult Compile(TypeOfAlert typeAlert, object relatedData, int? code, params string[] alertMessages)
        {
            return Compile(relatedData, code, new AlertSection(typeAlert, alertMessages));
        }
    }

    /// <summary>
    /// Класс представляет собой обертку над всеми контейнерами оповещений и общий результат операции
    /// </summary>
    public class AlertsWrapper
    {
        /// <summary>
        /// Флаг говорящий об успехе выполненой операции
        /// </summary>
        public bool Successed { get; set; }

        /// <summary>
        /// Сопуствующие данные которые необходимо передать с результатом операции
        /// </summary>
        public object RelatedData { get; set; }

        /// <summary>
        /// Любой код который необходимо отработать на клиенте
        /// </summary>
        public int Code { get; set; }

        public List<AlertSection> AlertSections { get; set; }

        public AlertsWrapper(List<AlertSection> alertSections)
        {
            AlertSections = alertSections ?? new List<AlertSection>();
            SetSuccessedByContentOfSections();
        }

        private void SetSuccessedByContentOfSections()
        {
            Successed = true;

            foreach (var section in AlertSections.Where(section => section != null && section.TypeAlert == TypeOfAlert.Error))
            {
                Successed = false;
                break;
            }
        }

        public AlertsWrapper()
        {
        }
    }

    /// <summary>
    /// Класс представляющий из себя контейнер с оповещениями одного типа
    /// </summary>
    public class AlertSection
    {
        public TypeOfAlert TypeAlert { get; set; }

        public string Message { get; set; }

        public string[] Messages { get; set; }

        public AlertSection(TypeOfAlert typeAlert, params string[] alertMessages)
        {
            TypeAlert = typeAlert;
            FitIntoTemplateLogic(alertMessages);
            SetDefaultMessages();
        }

        /// <summary>
        /// Метод подгоняющий текущий объект под необходимые условия для работы Хендлбар темплейта
        /// </summary>
        /// <param name="alertMessages"></param>
        private void FitIntoTemplateLogic(params string[] alertMessages)
        {
            if (alertMessages == null) return;
            if (alertMessages.Count() > 1)
            {
                Messages = alertMessages;
            }
            else
            {
                Message = alertMessages.FirstOrDefault();
            }
        }

        private void SetDefaultMessages()
        {
            var isAllMessagesEmpty = string.IsNullOrWhiteSpace(Message) && (Messages == null || Messages.All(string.IsNullOrWhiteSpace));
            if (isAllMessagesEmpty)
            {
                switch (TypeAlert)
                {
                    case TypeOfAlert.Info:
                        break;

                    case TypeOfAlert.Warning:
                        break;

                    case TypeOfAlert.Success:

                        break;

                    case TypeOfAlert.Error:
                        Message = "Внутренняя ошибка сервера";
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public AlertSection()
        {
        }

        public static AlertSection Success
        {
            get
            {
                return new AlertSection()
                {
                    TypeAlert = TypeOfAlert.Success
                };
            }
        }
    }
}