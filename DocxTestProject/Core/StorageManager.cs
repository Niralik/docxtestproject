﻿using DocxTestProject.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace DocxTestProject.Core
{
    public interface IFileStorageManager
    {
        string GetUrlToFile(string fileName);

        Stream GetFile(string fileName);

        List<DocxItemModel> GetUploadedDocxFiles();

        void SaveFile(Stream file, string fileName);

        void SaveFile(byte[] file, string fileName, bool isSource);
    }

    public class LocalStorageManager : IFileStorageManager
    {
        private const string uploadPath = "/Content/upload/";
        private static string phisicalUploadPath = "";
        private static string phisicalSourcePath = "";

        public LocalStorageManager()
        {
            if (HttpContext.Current == null)
            {
                return;
            }

            var path = HttpContext.Current.Server.MapPath("~");
            phisicalUploadPath = path + uploadPath;
            if (!Directory.Exists(phisicalUploadPath))
            {
                Directory.CreateDirectory(phisicalUploadPath);
            }
            phisicalSourcePath = phisicalUploadPath + "source/";
            if (!Directory.Exists(phisicalSourcePath))
            {
                Directory.CreateDirectory(phisicalSourcePath);
            }
        }

        public string GetUrlToFile(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName)) return string.Empty;
            return uploadPath + fileName;
        }

        public string GetPhisicalPathToFile(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName)) return string.Empty;
            return phisicalUploadPath + fileName;
        }

        public Stream GetSourceFile(string fileName)
        {
            return GetFile("source/" + fileName);
        }

        public Stream GetFile(string fileName)
        {
            var path = GetPhisicalPathToFile(fileName);
            return File.Open(path, FileMode.OpenOrCreate);
        }

        public List<DocxItemModel> GetUploadedDocxFiles()
        {
            var uploadFilesDirectory = new DirectoryInfo(phisicalUploadPath);
            var docxFiles = uploadFilesDirectory.EnumerateFiles("*.docx");
            return docxFiles.Select(docxfile => new DocxItemModel(GetUrlToFile(docxfile.Name), docxfile.Name)).ToList();
        }

        public void SaveFile(Stream file, string fileName)
        {
            SaveFile(file.ReadFully(), fileName);
        }

        public void SaveSourceFile(Stream file, string fileName)
        {
            SaveFile(file.ReadFully(), fileName, true);
        }

        public void SaveFile(byte[] file, string fileName, bool isSource = false)
        {
            if (isSource)
            {
                fileName = "source/" + fileName;
            }
            File.WriteAllBytes(GetPhisicalPathToFile(fileName), file);
        }
    }

    public class StorageWrapper
    {
        public static readonly Lazy<StorageWrapper> Instance = new Lazy<StorageWrapper>(() => new StorageWrapper());

        private readonly IFileStorageManager manager;

        public StorageWrapper()
        {
            manager = new LocalStorageManager();
            //manager = new AzureStorageManager();
        }

        public IFileStorageManager ImageStorageManager
        {
            get
            {
                return manager;
            }
        }
    }
}