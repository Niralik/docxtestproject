﻿using System;
using Novacode;
using System.Collections.Generic;
using System.Linq;
using WebGrease.Css.Extensions;

namespace DocxTestProject.Models
{
    public class DocxModel
    {
        public Dictionary<string, string> Fields { get; set; }

        public string Name { get; set; }

        private DocX Source { get; set; }

        public DocxModel(DocX rawDocxDocument, string name)
        {
            Fields = new Dictionary<string, string>();
            parseDocx(rawDocxDocument);
            Source = rawDocxDocument;
            Name = name;
        }

        public DocxModel()
        {
        }
        private void parseDocx(DocX rawDocxDocument)
        {
            var extractedFieldsName = rawDocxDocument.Text.Split('[', ']').Where((fieldName) => fieldName.Any() && !fieldName.Any(Char.IsWhiteSpace)).ToArray();
            extractedFieldsName.ForEach(HandleCoincedence);
        }

        private void HandleCoincedence(string fieldName)
        {
            var countCoincedence = Fields.Count(field => field.Key.Contains(fieldName));
            if (countCoincedence == 0)
            {
                Fields.Add(fieldName, "");
            }  
        }

        public DocX GetSource()
        {
            return Source;
        }

        public void Update(DocxModel docxModel)
        {
            foreach (var field in docxModel.Fields)
            {

                Source.ReplaceText("[" + field.Key + "]", field.Value + " ");
            }
        }
    }

    public class DocxItemModel
    {
        public string Name { get; set; }

        public string Path { get; set; }

        public DocxItemModel()
        {
        }

        public DocxItemModel(string path, string name)
        {
            Path = path;
            Name = name;
        }
    }
}